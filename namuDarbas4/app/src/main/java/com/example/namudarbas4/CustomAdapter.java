package com.example.gourav.GridViewExample;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.namudarbas4.R;

import org.w3c.dom.Text;

public class CustomAdapter extends BaseAdapter {
    Context context;
    int btns[];
    LayoutInflater inflter;
    public CustomAdapter(Context applicationContext, int[] btns) {
        this.context = applicationContext;
        this.btns = btns;
        inflter = (LayoutInflater.from(applicationContext));
    }
    @Override
    public int getCount() {
        return btns.length;
    }
    @Override
    public Object getItem(int i) {
        return null;
    }
    @Override
    public long getItemId(int i) {
        return 0;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.activity_gridview, null); // inflate the layout
        TextView btn = (TextView) view.findViewById(R.id.btn_1); // get the reference of ImageView
        btn.setText(Integer.toString(btns[i])); // set logo images
        return view;
    }
}