package com.example.namudarbas4;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class activity2 extends AppCompatActivity {
    TextView selectedBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity2);
        selectedBtn = (TextView) findViewById(R.id.selected_btn); // init a ImageView
        Intent intent = getIntent(); // get Intent which we set from Previous Activity
        selectedBtn.setText(Integer.toString(intent.getIntExtra("btn_no", 0)));
    }
}
