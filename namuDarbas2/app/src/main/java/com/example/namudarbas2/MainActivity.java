package com.example.namudarbas2;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String name;
    Button submit;
    TextView text;
    EditText textname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text = findViewById(R.id.textview);
        textname = findViewById(R.id.edittext);

        submit = findViewById(R.id.btn);

        submit.setOnClickListener(this);


    }

    public void onClick(View view){
        name = String.format("%1$s, %2$s%3$s",getText(R.string.greeting),textname.getText().toString(),getText(R.string.exclamation));
        text.setText(name);
    }

}
